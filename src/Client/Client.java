package Client;

import java.net.Socket;

public class Client {
	public static void main(String args[]){
		try {
			Socket sock = new Socket("localhost",8080);
			Sender sendThread = new Sender(sock);
			sendThread.start();
			Receiver receiveThread = new Receiver(sock);
			receiveThread.start();
		} catch (Exception e) {System.out.println(e.getMessage());} 

	}
	
}
