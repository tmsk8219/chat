package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server{
	public static void main(String args[]) throws IOException{
		final int port = 8080;
		System.out.println("Server waiting for connection on port "+port);
		ServerSocket ss = new ServerSocket(port);
		Socket clientSocket = ss.accept();
		System.out.println("Recieved connection from "+clientSocket.getInetAddress()+" on port "+clientSocket.getPort());
		//create two threads to send and recieve from client
		Receiver receive = new Receiver(clientSocket);
		receive.start();
		Sender send = new Sender(clientSocket);
		send.start();
	}
}
